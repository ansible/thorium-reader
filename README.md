# Thorium desktop reader

This Ansible role installs Thorium reader using its DEB installer.
Apart from being able to install the latest version or a specified earlier version,
this role does nothing fancy.

Tested on Ubuntu 22.04.


## To-do

+ Explore Thorium's audiobook functionality. Does the desktop reader support audiobooks?
+ Test [the Android app](https://www.edrlab.org/software/readium-mobile/).
  Can it already replace Librera Reader?


## Links and notes

+ https://www.edrlab.org/software/thorium-reader
